﻿using System;

namespace BSS
{
	public partial class EnsureThat
	{
		public void IsNull<T>(T value)
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (value != null)
			{
				throw new ArgumentNullException(paramName, ExceptionMessages.Common_IsNull_Failed);
			}
		}


        public void IsNotNull<T>(T value)
		{
			if (!Ensure.IsActive)
			{
				return;
			}

			if (value == null)
			{
				throw new ArgumentNullException(paramName, ExceptionMessages.Common_IsNotNull_Failed);
			}
		}
	}
}
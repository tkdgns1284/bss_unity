namespace BSS
{
	public static class Ensure
	{
		private static readonly EnsureThat instance = new EnsureThat();

        public static bool IsActive { get; set; } = true;

		public static void Off() => IsActive = false;

		public static void On() => IsActive = true;

		public static EnsureThat That(string paramName)
		{
			instance.paramName = paramName;
			return instance;
		}
	}
}
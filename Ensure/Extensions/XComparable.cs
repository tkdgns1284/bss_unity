using System;

namespace BSS
{
	internal static class XComparable
	{
		internal static bool IsLessThan<T>(this IComparable<T> x, T y)
		{
			return x.CompareTo(y) < 0;
		}

		internal static bool IsEqual<T>(this IComparable<T> x, T y)
		{
			return x.CompareTo(y) == 0;
		}

		internal static bool IsGreaterThan<T>(this IComparable<T> x, T y)
		{
			return x.CompareTo(y) > 0;
		}
	}
}
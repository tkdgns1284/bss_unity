﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

namespace BSS.View {
    public static class ViewUtility {
        internal static List<BaseView> baseViewAll = new List<BaseView>();


        public static T Find<T>() where T : BaseView {
            return baseViewAll.Find(x => x is T) as T;
        }
        public static T Find<T>(string ID) where T : BaseView {
            return baseViewAll.Find(x =>x is T && x.ID == ID) as T;
        }
        public static T Find<T>(Func<T,bool> predicate) where T : BaseView {
            return baseViewAll.Find(x => x is T && predicate(x as T)) as T;
        }
        public static T Show<T>() where T : BaseView {
            var view = Find<T>();
            view.Show();
            return view;
        }
        public static T Show<T>(string ID) where T : BaseView {
            var view = Find<T>(ID);
            view.Show();
            return view;
        }
        public static T Close<T>() where T : BaseView {
            var view = Find<T>();
            view.Close();
            return view;
        }
        public static T Close<T>(string ID) where T : BaseView {
            var view = Find<T>(ID);
            view.Close();
            return view;
        }

        public static List<T> FindAll<T>() where T : BaseView {
            return baseViewAll.FindAll(x => x is T).ConvertAll(x => x as T);
        }

    }
}

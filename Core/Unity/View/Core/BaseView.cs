﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine.UI;
using System;


namespace BSS.View {
#if ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif


    [RequireComponent (typeof(CanvasGroup))]
	public class BaseView : MonoBehaviour
	{
		[SerializeField]
		private string id;

		public string ID {
			get{ return id; }
			set{ id = value; }
		}

        public event Action OnShowAction;
        public event Action OnCloseAction;

        public bool IsVisible { 
			get { 
				return canvasGroup.alpha == 1f; 
			} 
		}
        public int sortingOrder => parentCanvas.sortingOrder;
        public string sortingLayerName => parentCanvas.sortingLayerName;
        public int sortingLayerID => parentCanvas.sortingLayerID;

        protected Canvas parentCanvas=> GetComponentInParent<Canvas>();
        protected RectTransform rectTransform => GetComponent<RectTransform>();
        protected CanvasGroup canvasGroup=> GetComponent<CanvasGroup>();


        private void OnEnable() {
            ViewUtility.baseViewAll.Add(this);
        }
        private void OnDisable() {
            ViewUtility.baseViewAll.Remove(this);
        }


		public virtual void Show ()
		{
			canvasGroup.interactable = true;
			canvasGroup.blocksRaycasts = true;
            canvasGroup.alpha = 1f;

            if (OnShowAction != null) {
                OnShowAction.Invoke ();
			}
		}

		public virtual void Close ()
		{
			canvasGroup.interactable = false;
			canvasGroup.blocksRaycasts = false;
            canvasGroup.alpha = 0f;

            if (OnCloseAction != null) {
                OnCloseAction.Invoke ();
			}
		}
        public virtual void Toggle() {
            if(!IsVisible) {
                Show();
            } else {
                Close();
            }
        }
        public T NextView<T>(bool selfClose = false) where T : BaseView {
            var view = ViewUtility.Find<T>();
            view.Show();
            if(selfClose) {
                Close();
            }
            return view;
        }

        #if ODIN_INSPECTOR
        [ButtonGroup("ShowClose")]
        [Button(Name = "Show")]
        #endif
        public void ShowOnlyCanvasGroup() {
            GetComponent<CanvasGroup>().alpha = 1f;
            GetComponent<CanvasGroup>().interactable = true;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
        #if ODIN_INSPECTOR
        [ButtonGroup("ShowClose")]
        [Button(Name ="Close")]
        #endif
        public void CloseOnlyCanvasGroup() {
            GetComponent<CanvasGroup>().alpha = 0f;
            GetComponent<CanvasGroup>().interactable = false;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
        
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace BSS {
    /// <summary>
    /// 숫자 텍스트가 순차적으로 증가하는 UI
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class IncreaseText : MonoBehaviour {
        [Header("Data")]
        public long minNumber = 0;
        public long maxNumber = 1;

        public float range {
            get => _range;
            set => _range = Mathf.Clamp01(value);
        }
        [SerializeField]
        [Range(0f,1f)]
        private float _range;

        [Header("Text")]
        public string textFormat = "0";

        private long curValue => minNumber + (long)((float)(maxNumber - minNumber) * range);
        private long applyValue = 0;

        private Text mText=> GetComponent<Text>();

        private void OnValidate() {
            UpdateText();
        }

        private void Start() {
            UpdateText();
        }

        private void Update() {
            if (applyValue == curValue) return;
            UpdateText();
        }

        private void UpdateText() {
            applyValue = curValue;
            mText.text = curValue.ToString(textFormat);
        }
    }
}

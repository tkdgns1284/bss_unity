﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BSS {
    [DisallowMultipleComponent]
    [RequireComponent(typeof(RectTransform))]
    public class ClickableUI : MonoBehaviour,IPointerDownHandler,IPointerUpHandler,IPointerClickHandler {
        public static float LONG_INTERVAL = 0.8f;
        public static float DOUBLE_INTERVAL = 0.2f;

        public bool isOnce = true;
        public bool isDouble;
        public bool isLong;


        public event Action OnOnceClicked;
        public event Action OnDoubleClicked;
        public event Action OnLongClicked;

        private int downCount;
        private float clickTime;

        private Coroutine doubleCoroutine;
        private Coroutine longCoroutine;


        public ClickableUI RegistInOnce(Action act) {
            isOnce = true;
            OnOnceClicked += act;
            return this;
        }
        public ClickableUI RegistInDouble(Action act) {
            isDouble = true;
            OnDoubleClicked += act;
            return this;
        }
        public ClickableUI RegistInLong(Action act) {
            isLong = true;
            OnLongClicked += act;
            return this;
        }
        public ClickableUI ClearInOnce(Action act) {
            OnOnceClicked = null;
            return this;
        }
        public ClickableUI ClearInDouble(Action act) {
            OnDoubleClicked = null;
            return this;
        }
        public ClickableUI ClearInLong(Action act) {
            OnLongClicked = null;
            return this;
        }

        public void OnPointerDown(PointerEventData eventData) {
            clickTime = 0f;
            downCount++;

            if(isOnce && !isDouble && !isLong) {
                OnOnceClicked?.Invoke();
                return;
            }
            if(isDouble && doubleCoroutine == null) {
                doubleCoroutine = StartCoroutine(CoWaitDouble());
            }
            if(isLong && longCoroutine == null) {
                longCoroutine = StartCoroutine(CoWaitLong());
            }
        }
        public void OnPointerUp(PointerEventData eventData) {
            clickTime = 0f;
        }
        public void OnPointerClick(PointerEventData eventData) {
            clickTime += Time.deltaTime;
        }



        private IEnumerator CoWaitDouble() {
            float startTime = Time.time;
            int curCount = downCount;
            while(true) {
                if(curCount < downCount) {
                    OnDoubleClicked?.Invoke();
                    break;
                }

                if(startTime + DOUBLE_INTERVAL < Time.time) {
                    if(longCoroutine == null) {
                        OnOnceClicked?.Invoke();
                    }
                    break;
                }
                yield return null;
            }
            doubleCoroutine = null;
        }
        private IEnumerator CoWaitLong() {
            float startTime = Time.time;
            while(true) {
                if(!Input.GetMouseButton(0)) {
                    if(startTime + DOUBLE_INTERVAL < Time.time) {
                        OnOnceClicked?.Invoke();
                    }
                    break;
                }

                if(startTime + LONG_INTERVAL < Time.time) {
                    OnLongClicked?.Invoke();
                    break;
                }

                yield return null;
            }
            longCoroutine = null;
        }

        
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Collider2D))]
    public class Clickable : MonoBehaviour {
        public static float LONG_INTERVAL = 0.8f;
        public static float DOUBLE_INTERVAL = 0.2f;

        public bool isOnce=true;
        public bool isDouble;
        public bool isLong;
        

        public event Action OnOnceClicked;
        public event Action OnDoubleClicked;
        public event Action OnLongClicked;

        private int downCount;
        private float clickTime;

        private Coroutine doubleCoroutine;
        private Coroutine longCoroutine;


        public Clickable RegistInOnce(Action act) {
            isOnce = true;
            OnOnceClicked += act;
            return this;
        }
        public Clickable RegistInDouble(Action act) {
            isDouble = true;
            OnDoubleClicked += act;
            return this;
        }
        public Clickable RegistInLong(Action act){
            isLong = true;
            OnLongClicked += act;
            return this;
        }
        public Clickable ClearInOnce(Action act) {
            OnOnceClicked = null;
            return this;
        }
        public Clickable ClearInDouble(Action act) {
            OnDoubleClicked = null;
            return this;
        }
        public Clickable ClearInLong(Action act){
            OnLongClicked = null;
            return this;
        }

        private void OnMouseDrag(){
            clickTime += Time.deltaTime;
        }
        private void OnMouseUp(){
            clickTime = 0f;
        }

        private void OnMouseDown() {
            clickTime = 0f;
            downCount++;

            if (isOnce && !isDouble && !isLong) {
                OnOnceClicked?.Invoke();
                return;
            } 
            if (isDouble && doubleCoroutine==null) {
                doubleCoroutine=StartCoroutine(CoWaitDouble());
            }
            if(isLong && longCoroutine==null) {
                longCoroutine=StartCoroutine(CoWaitLong());
            }
        }
        




        private IEnumerator CoWaitDouble() {
            float startTime = Time.time;
            int curCount = downCount;
            while (true) {
                if (curCount< downCount) {
                    OnDoubleClicked?.Invoke();
                    break;
                }

                if (startTime+ DOUBLE_INTERVAL < Time.time) {
                    if (longCoroutine==null) {
                        OnOnceClicked?.Invoke();
                    }
                    break;
                }
                yield return null;
            }
            doubleCoroutine = null;
        }
        private IEnumerator CoWaitLong(){
            float startTime = Time.time;
            while (true) {
                if (!Input.GetMouseButton(0)) {
                    if (startTime+DOUBLE_INTERVAL<Time.time) {
                        OnOnceClicked?.Invoke();
                    }
                    break;
                }

                if (startTime+ LONG_INTERVAL < Time.time) {
                    OnLongClicked?.Invoke();
                    break;
                }

                yield return null;
            }
            longCoroutine = null;
        }
    }
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BSS {
    [System.Serializable]
    public class WorldTextInfo {
        [Header("Canvas")]
        public Vector2 canvasScale = Vector2.one * 0.01f;
        public Vector2 canvasSize = new Vector2(200f, 200f);
        public Vector2 relativePosition;
        public string sortingLayerName = "Default";
        public int sortingOrder;

        [Header("Font")]
        public Font font;
        public int fontSize = 10;
        public string content = "";
        public Color fontColor = Color.white;
        public TextAnchor alignment = TextAnchor.MiddleCenter;
    }
    /// <summary>
    /// GameObject에 Text를 표시 할 수 있게 해주는 클래스
    /// </summary>
    public class WorldTextDrawer : MonoBehaviour {
        [SerializeField]
        private WorldTextInfo startInfo = new WorldTextInfo();

        public Canvas canvas {
            private set;get;
        }
        public string text {
            set => textUI.text = value;
            get => textUI.text; 
        }

        public Text textUI {
            get;private set;
        }

        private void Awake() {
            //Regist
            canvas = Go.New("Canvas").SetParent(gameObject, startInfo.relativePosition).Attach<Canvas>();
            canvas.worldCamera = Camera.main;
            textUI = Go.New("Text").SetParent(canvas.transform).Attach<Text>();
            textUI.GetComponent<RectTransform>().SetFullSize();
            textUI.raycastTarget = false;

            UpdateInfo(startInfo);
        }


        private void UpdateInfo(WorldTextInfo info) {
            canvas.transform.localScale = info.canvasScale;
            canvas.GetComponent<RectTransform>().SetSize(info.canvasSize);
            canvas.sortingLayerName = info.sortingLayerName;
            canvas.sortingOrder = info.sortingOrder;
            textUI.font = info.font;
            textUI.fontSize = info.fontSize;
            textUI.text = info.content;
            textUI.color = info.fontColor;
            textUI.alignment = info.alignment;
        }
    }
}

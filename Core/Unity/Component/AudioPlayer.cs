﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BSS{
    public class AudioPlayer : MonoBehaviour{
        [Range(1, 20)]
        public int playMaxCount = 10;
        [Range(0f, 1f)]
        public float baseBgmVolume = 1f;
        [Range(0f, 1f)]
        public float baseEffectVolume = 1f;
        public List<AudioClip> clips = new List<AudioClip>();

        private AudioSource bgmSource;
        private List<AudioSource> audioSources = new List<AudioSource>();

        private bool isInit;

        public void Awake() {
            bgmSource = gameObject.AddComponent<AudioSource>();
            bgmSource.loop = true;
            bgmSource.playOnAwake = false;
            for(int i = 0; i < playMaxCount; i++) {
                audioSources.Add(CreateChild());
            }
        }
        /// <summary>
        /// 현재 재생중인 배경 음악을 가져옵니다.
        /// </summary>
        public AudioClip GetPlayingBgmClip() {
            if(!bgmSource.isPlaying)
                return null;
            return bgmSource.clip;
        }

        /// <summary>
        /// 배경 음악을 설정하고 재생합니다.
        /// </summary>
        public void SetBgm(AudioClip clip, float volume = 1f) {
            if(bgmSource.isPlaying) {
                if(clip.name == bgmSource.clip.name)
                    return;
                bgmSource.Stop();
            }
            bgmSource.volume = baseBgmVolume * volume;
            bgmSource.clip = clip;
            bgmSource.Play();
        }


        /// <summary>
        /// 배경 음악의 볼륨을 조절합니다.
        /// </summary>
        public void ChangeBgmVolume(float volume) {
            baseBgmVolume = volume;
            bgmSource.volume = baseBgmVolume * volume;
        }

        /// <summary>
        /// 모든 효과음의 볼륨을 조절합니다.
        /// </summary>
        public void ChangeEffectVolume(float volume) {
            baseEffectVolume = volume;
            audioSources.ForEach(x => x.volume = baseEffectVolume * volume);
        }

        /// <summary>
        /// 배경 음악을 재개합니다.
        /// </summary>
        public void ResumeBgm() {
            bgmSource.Play();
        }
        /// <summary>
        /// 배경 음악을 멈춥니다.
        /// </summary>
        public void PauseBgm() {
            bgmSource.Pause();
        }

        /// <summary>
        /// 사운드를 1회 재생합니다.
        /// </summary>
        public AudioSource PlayOnce(AudioClip clip, float volume = 1f) {
            var audioSource = audioSources.Find(x => !x.isPlaying);
            if(audioSource == null)
                return null;
            audioSource.loop = false;
            audioSource.clip = clip;
            audioSource.volume = baseEffectVolume * volume;
            audioSource.Play();
            return audioSource;
        }

        /// <summary>
        /// 사운드를 반복 재생합니다.
        /// </summary>
        public AudioSource PlayLoop(AudioClip clip, float volume = 1f) {
            var audioSource = audioSources.Find(x => !x.isPlaying);
            if(audioSource == null)
                return null;
            audioSource.loop = true;
            audioSource.clip = clip;
            audioSource.volume = baseEffectVolume * volume;
            audioSource.Play();
            return audioSource;
        }

        /// <summary>
        /// 사운드를 일정 시간동안 반복 재생합니다.
        /// </summary>
        public void PlayLoopInTime(AudioClip clip, float loopTime, float volume = 1f) {
            var source = PlayLoop(clip, volume);
            StartCoroutine(CoExcuteAfterSeconds(loopTime, () => {
                if(source == null || !source.isPlaying || source.clip != clip)
                    return;
                source.Stop();
            }));
        }

        /// <summary>
        /// 사운드를 특정 조건이 만족하기 전까지 반복 재생합니다.
        /// </summary>
        public void PlayLoopInCondition(AudioClip clip, System.Func<bool> endCondition, float volume = 1f) {
            var source = PlayLoop(clip, volume);
            StartCoroutine(CoExcuteAfterCondition(endCondition, () => {
                if(source == null || !source.isPlaying || source.clip != clip)
                    return;
                source.Stop();
            }));
        }


        IEnumerator CoExcuteAfterSeconds(float seconds, Action act) {
            yield return new WaitForSeconds(seconds);
            act?.Invoke();
        }
        IEnumerator CoExcuteAfterCondition(Func<bool> condition, Action act) {
            yield return new WaitUntil(condition);
            act?.Invoke();
        }


        private AudioSource CreateChild() {
            var obj = new GameObject("Sound Player");
            obj.transform.SetParent(transform);
            var source = obj.AddComponent<AudioSource>();
            return source;
        }

    }
}


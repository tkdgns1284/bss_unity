﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public class DestroyTimer : MonoBehaviour {
        public float time=10f;

        public DestroyTimer SetTime(float sec) {
            time = sec;
            return this;
        }
        public DestroyTimer AddTime(float sec) {
            time += sec;
            return this;
        }

        private void Update() {
            if (time < 0f) {
                Destroy(gameObject);
                return;
            }
            time -= Time.deltaTime;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Linq;
using Random = UnityEngine.Random;

namespace BSS {
    public static class RandomUtility 
    {
        public static int WeightedRandom(IEnumerable<int> list) {
            int sum = list.Sum();
            int r = Random.Range(0, sum);
            int checkVal = 0;
            int i = 0;
            foreach (var it in list) {
                checkVal += it;
                if(r < checkVal) {
                    return i;
                }
                i++;
            }
            return i-1;
        }

        public static int WeightedRandom(IEnumerable<float> list) {
            float sum = list.Sum();
            float r = Random.Range(0, sum);
            float checkVal = 0;
            int i = 0;
            foreach(var it in list) {
                checkVal += it;
                if(r < checkVal) {
                    return i;
                }
                i++;
            }
            return i - 1;
        }
        public static Color RandomColor(float a) {
            return new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), a);
        }
    }
}

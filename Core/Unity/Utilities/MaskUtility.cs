﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class MaskUtility 
    {
        public static SpriteMask SetScaleLikeRect(this SpriteMask sprMask, Rect rect) {
            Ensure.That(nameof(sprMask.sprite)).IsNotNull(sprMask.sprite);
            sprMask.transform.localScale = (Vector3)Vector2Utility.Devide(rect.size, (Vector2)sprMask.sprite.bounds.size);
            sprMask.transform.localScale = sprMask.transform.localScale.SetZ(1f);
            return sprMask;
        }
        public static SpriteMask SetScaleLikeBounds(this SpriteMask sprMask, Bounds bounds) {
            Ensure.That(nameof(sprMask.sprite)).IsNotNull(sprMask.sprite);
            sprMask.transform.localScale = (Vector3)Vector2Utility.Devide(bounds.size, sprMask.sprite.bounds.size);
            sprMask.transform.localScale = sprMask.transform.localScale.SetZ(1f);
            return sprMask;
        }
    }
}

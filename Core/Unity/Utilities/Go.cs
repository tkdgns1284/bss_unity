﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BSS {
    public static class Go {
        public static GameObject New() {
            var go = new GameObject();
            return go;
        }
        public static GameObject New(string name) {
            var go = new GameObject();
            go.name = name;
            return go;
        }
        public static T NewComp<T>() where T : Component {
            var go = new GameObject();
            return go.AddComponent<T>();
        }
        public static T NewComp<T>(string name) where T : Component {
            var go = new GameObject(name);
            return go.AddComponent<T>();
        }

        public static void DestroyAllGameObjects(this IEnumerable<GameObject> source) {
            foreach(var it in source) {
                Object.Destroy(it);
            }
        }
        public static void DestroyAllGameObjects(IEnumerable<Component> source) {
            foreach(var it in source) {
                Object.Destroy(it.gameObject);
            }
        }
        public static void DestroyAllComponents(IEnumerable<Component> source) {
            foreach(var it in source) {
                Object.Destroy(it);
            }
        }

    }

    public static class GameObjectEx {
        public static GameObject SetName(this GameObject go, string name) {
            go.name = name;
            return go;
        }

        public static GameObject NewChild(this GameObject go) {
            return Go.New().SetParent(go);
        }
        public static GameObject NewChild(this GameObject go, string name) {
            return Go.New(name).SetParent(go);
        }

        public static GameObject GetChild(this GameObject go, int index) {
            return go.transform.GetChild(index).gameObject;
        }
        public static GameObject GetChild(this GameObject go, string name) {
            return Enumerable.Range(0, go.transform.childCount).
                Select(x => go.transform.GetChild(x)).First(x => x.name == name).gameObject;
        }

        public static T Attach<T>(this GameObject go) where T : Component {
            var component = go.GetComponent<T>();
            if(component == null) {
                return go.AddComponent<T>();
            }
            return component;
        }
        public static GameObject Detach<T>(this GameObject go) where T : Component {
            Object.Destroy(go.GetComponent<T>());
            return go;
        }

        public static T Get<T>(this GameObject go) where T : Component {
            return go.GetComponent<T>();
        }

        public static bool Has<T>(this GameObject go) where T : Component {
            return go.GetComponent<T>() != null;
        }



        public static GameObject SetParent(this GameObject go, Transform parent, Vector3 localPosition) {
            go.transform.SetParent(parent, false);
            go.transform.localPosition = localPosition;
            return go;
        }
        public static GameObject SetParent(this GameObject go, Transform parent, bool isStay = false) {
            go.transform.SetParent(parent, isStay);
            return go;
        }
        public static GameObject SetParent(this GameObject go, GameObject parentObj, bool isStay = false) {
            return SetParent(go, parentObj.transform, isStay);
        }
        public static GameObject SetParent(this GameObject go, GameObject parentObj, Vector3 localPosition) {
            return SetParent(go, parentObj.transform, localPosition);
        }

        public static GameObject SetPos(this GameObject go, Vector3 worldPos) {
            go.transform.position = worldPos;
            return go;
        }
        public static GameObject SetPos(this GameObject go, Vector2 worldPos) {
            go.transform.position = worldPos;
            return go;
        }
        public static GameObject SetPos(this GameObject go, float x, float y, float z) {
            go.transform.position = new Vector3(x, y, z);
            return go;
        }
        public static GameObject SetPos(this GameObject go, float x, float y) {
            go.transform.position = new Vector3(x, y, 0f);
            return go;
        }
        public static GameObject SetPosX(this GameObject go, float val) {
            go.transform.position = new Vector3(val, go.transform.position.y, go.transform.position.z);
            return go;
        }
        public static GameObject SetPosY(this GameObject go, float val) {
            go.transform.position = new Vector3(go.transform.position.x, val, go.transform.position.z);
            return go;
        }
        public static GameObject SetPosZ(this GameObject go, float val) {
            go.transform.position = new Vector3(go.transform.position.x, go.transform.position.y, val);
            return go;
        }

        public static GameObject SetLocalPos(this GameObject go, Vector3 localPos) {
            go.transform.localPosition = localPos;
            return go;
        }
        public static GameObject SetLocalPos(this GameObject go, Vector2 localPos) {
            go.transform.localPosition = localPos;
            return go;
        }
        public static GameObject SetLocalPos(this GameObject go, float x, float y, float z) {
            go.transform.localPosition = new Vector3(x, y, z);
            return go;
        }
        public static GameObject SetLocalPos(this GameObject go, float x, float y) {
            go.transform.localPosition = new Vector3(x, y, 0f);
            return go;
        }
        public static GameObject SetLocalPosX(this GameObject go, float val) {
            go.transform.localPosition = new Vector3(val, go.transform.localPosition.y, go.transform.localPosition.z);
            return go;
        }
        public static GameObject SetLocalPosY(this GameObject go, float val) {
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, val, go.transform.localPosition.z);
            return go;
        }
        public static GameObject SetLocalPosZ(this GameObject go, float val) {
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y, val);
            return go;
        }

        public static GameObject AddPos(this GameObject go, Vector3 worldPos) {
            go.transform.position += worldPos;
            return go;
        }
        public static GameObject AddPos(this GameObject go, Vector2 worldPos) {
            go.transform.position += (Vector3)worldPos;
            return go;
        }
        public static GameObject AddPos(this GameObject go, float x, float y, float z) {
            go.transform.position += new Vector3(x, y, z);
            return go;
        }
        public static GameObject AddPos(this GameObject go, float x, float y) {
            go.transform.position += new Vector3(x, y, 0f);
            return go;
        }
        public static GameObject AddPosX(this GameObject go, float val) {
            go.transform.position += new Vector3(val, 0f, 0f);
            return go;
        }
        public static GameObject AddPosY(this GameObject go, float val) {
            go.transform.position += new Vector3(0f, val, 0f);
            return go;
        }
        public static GameObject AddPosZ(this GameObject go, float val) {
            go.transform.position += new Vector3(0f, 0f, val);
            return go;
        }

        public static GameObject AddLocalPos(this GameObject go, Vector3 worldPos) {
            go.transform.localPosition += worldPos;
            return go;
        }
        public static GameObject AddLocalPos(this GameObject go, Vector2 worldPos) {
            go.transform.localPosition += (Vector3)worldPos;
            return go;
        }
        public static GameObject AddLocalPos(this GameObject go, float x, float y, float z) {
            go.transform.localPosition += new Vector3(x, y, z);
            return go;
        }
        public static GameObject AddLocalPos(this GameObject go, float x, float y) {
            go.transform.localPosition += new Vector3(x, y, 0f);
            return go;
        }
        public static GameObject AddLocalPosX(this GameObject go, float val) {
            go.transform.localPosition += new Vector3(val, 0f, 0f);
            return go;
        }
        public static GameObject AddLocalPosY(this GameObject go, float val) {
            go.transform.localPosition += new Vector3(0f, val, 0f);
            return go;
        }
        public static GameObject AddLocalPosZ(this GameObject go, float val) {
            go.transform.localPosition += new Vector3(0f, 0f, val);
            return go;
        }

        public static GameObject SetScale(this GameObject go, Vector3 size) {
            go.transform.localScale = size;
            return go;
        }
        public static GameObject SetScale(this GameObject go, Vector2 size) {
            go.transform.localScale = new Vector3(size.x, size.y, 1f);
            return go;
        }
        public static GameObject SetScale(this GameObject go, float x, float y, float z) {
            go.transform.localScale = new Vector3(x, y, z);
            return go;
        }
        public static GameObject SetScale(this GameObject go, float x, float y) {
            go.transform.localScale = new Vector3(x, y, 1f);
            return go;
        }
        public static GameObject SetScaleX(this GameObject go, float val) {
            go.transform.localScale = new Vector3(val, go.transform.localScale.y, go.transform.localScale.z);
            return go;
        }
        public static GameObject SetScaleY(this GameObject go, float val) {
            go.transform.localScale = new Vector3(go.transform.localScale.x, val, go.transform.localScale.z);
            return go;
        }
        public static GameObject SetScaleZ(this GameObject go, float val) {
            go.transform.localScale = new Vector3(go.transform.localScale.x, go.transform.localScale.y, val);
            return go;
        }
    }

    public static class ComponentEx {
        public static T As<T>(this Component comp) where T : Component {
            if(!(comp is T)) throw new System.Exception($"{nameof(comp)} Type is Not {nameof(T)}!");
            return comp as T;
        }
        public static T Attach<T>(this Component comp) where T : Component {
            var component = comp.GetComponent<T>();
            if(component == null) {
                return comp.gameObject.AddComponent<T>();
            }
            return component;
        }
        public static Component Detach<T>(this Component comp) where T : Component {
            Object.Destroy(comp.GetComponent<T>());
            return comp;
        }
        public static T Get<T>(this Component comp) where T : Component {
            return comp.GetComponent<T>();
        }

        public static bool Has<T>(this Component comp) where T : Component {
            return comp.GetComponent<T>() != null;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class RectUtility 
    {
        public enum StartPoint {
            LeftBottom=0,
            LeftTop=1,
        }

        /// <summary>
        /// Rect를 특정 사이즈로 균등하게 자른 후 해당 위치의 Rect를 가져옵니다.
        /// </summary>
        /// <param name="startPoint">시작 위치 (x=0,y=0)</param>
        /// <returns></returns>
        public static Rect Split(this Rect rect, int splitX, int splitY, int x, int y, StartPoint startPoint) {
            Ensure.That(nameof(rect.size.x)).IsGreater(rect.size.x, 0);
            Ensure.That(nameof(rect.size.y)).IsGreater(rect.size.y, 0);
            Ensure.That(nameof(splitX)).IsGreater(splitX, 0);
            Ensure.That(nameof(splitY)).IsGreater(splitY, 0);
            Ensure.That(nameof(x)).IsInRange(x, 0, splitX);
            Ensure.That(nameof(y)).IsInRange(y, 0, splitY);
            if (startPoint==StartPoint.LeftTop) {
                y = (splitY-1) - y;
            }
            Vector2 rectSize = Vector2Utility.Devide(rect.size, new Vector2(splitX, splitY));
            Vector2 firstPos =rect.min+Vector2.Scale(rectSize, new Vector2(x, y));
            return new Rect(firstPos, rectSize);
        }
    }
}

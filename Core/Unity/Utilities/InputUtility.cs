﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class InputUtility 
    {
        public static Vector2 GetWolrdMousePos() {
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}

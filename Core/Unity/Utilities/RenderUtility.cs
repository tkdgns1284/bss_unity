﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class RenderUtility 
    {
        public static SpriteRenderer SetSprite(this SpriteRenderer sprRender, Sprite spr) {
            sprRender.sprite = spr;
            return sprRender;
        }
        public static SpriteRenderer SetScaleLikeBounds(this SpriteRenderer sprRender,Bounds bounds) {
            Ensure.That(nameof(sprRender.sprite)).IsNotNull(sprRender.sprite);
            sprRender.transform.localScale = Vector3Utility.Devide(bounds.size, sprRender.sprite.bounds.size);
            sprRender.transform.localScale = sprRender.transform.localScale.SetZ(1f);
            return sprRender;
        }
        public static SpriteRenderer SetScaleLikeRect(this SpriteRenderer sprRender, Rect rect) {
            Ensure.That(nameof(sprRender.sprite)).IsNotNull(sprRender.sprite);
            sprRender.transform.localScale = (Vector3)Vector2Utility.Devide(rect.size, sprRender.sprite.bounds.size);
            sprRender.transform.localScale = sprRender.transform.localScale.SetZ(1f);
            return sprRender;
        }
        public static SpriteRenderer SetOrder(this SpriteRenderer sprRender, int order) {
            sprRender.sortingOrder = order;
            return sprRender;
        }
        public static SpriteRenderer SetLayer(this SpriteRenderer sprRender, string layerName) {
            Ensure.That(nameof(layerName)).IsNotNullOrEmpty(layerName);
            sprRender.sortingLayerName = layerName;
            return sprRender;
        }
        public static SpriteRenderer SetLayer(this SpriteRenderer sprRender, int layerID) {
            sprRender.sortingLayerID = layerID;
            return sprRender;
        }
        public static SpriteRenderer SetColor(this SpriteRenderer sprRender, Color color) {
            sprRender.color = color;
            return sprRender;
        }
        public static SpriteRenderer SetAlpha(this SpriteRenderer sprRender, float alpha) {
            sprRender.color = new Color(sprRender.color.r, sprRender.color.g, sprRender.color.b, alpha);
            return sprRender;
        }
        public static SpriteRenderer SetFlipX(this SpriteRenderer sprRender, bool onOff) {
            sprRender.flipX = onOff;
            return sprRender;
        }
        public static SpriteRenderer SetFlipY(this SpriteRenderer sprRender, bool onOff) {
            sprRender.flipY = onOff;
            return sprRender;
        }
    }
}

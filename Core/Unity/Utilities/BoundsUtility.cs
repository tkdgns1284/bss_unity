﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    public static class BoundsUtility {
        public static Rect ToRect(this Bounds bounds) {
            return new Rect(bounds.min, bounds.size);
        }

        public static SpriteMask SetScaleLikeBounds(this SpriteMask sprMask, Bounds bounds) {
            Ensure.That(nameof(sprMask.sprite)).IsNotNull(sprMask.sprite);
            sprMask.transform.localScale = (Vector3)Vector3Utility.Devide(bounds.size, sprMask.sprite.bounds.size);
            sprMask.transform.localScale = sprMask.transform.localScale.SetZ(1f);
            return sprMask;
        }
    }

}
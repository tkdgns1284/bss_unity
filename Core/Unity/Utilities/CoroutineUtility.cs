﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BSS {
    /// <summary>
    /// 코루틴 함수를 쉽게 사용하게 하는 클래스
    /// (주의: 씬이 변경되도 반드시 실행됨 -대안: MonoBehaviour.ExtensionMethod)
    /// </summary>
    public static class CoroutineUtility  {
        private class CoroutineHandler :MonoBehaviour{}

        private static CoroutineHandler coHandler => coHandlerObj.GetComponent<CoroutineHandler>();
        private static GameObject _coHandlerObj;
        private static GameObject coHandlerObj {
            get {
                if (_coHandlerObj == null) {
                    _coHandlerObj = new GameObject("CoroutineHandler");
                    UnityEngine.Object.DontDestroyOnLoad(_coHandlerObj);
                    _coHandlerObj.AddComponent<CoroutineHandler>();
                }
                return _coHandlerObj;
            }
        }

        public static Coroutine ExcuteAfterSeconds(float seconds, Action act) {
            return coHandler.StartCoroutine(CoExcuteAfterSeconds(seconds, act)); ;
        }
        public static Coroutine ExcuteAfterSeconds(this MonoBehaviour mono, float sec, Action action) {
            return mono.StartCoroutine(CoExcuteAfterSeconds(sec, action));
        }

        public static Coroutine ExcuteAfterCondition(Func<bool> condition,Action act) {
            return coHandler.StartCoroutine(CoExcuteAfterCondition(condition, act));
        }
        public static Coroutine ExcuteAfterCondition(this MonoBehaviour mono, Func<bool> condition, Action action) {
            return mono.StartCoroutine(CoExcuteAfterCondition(condition, action));
        }

        
        public static Coroutine ExcuteAfterFrame(int frameCount,Action act) {
            return coHandler.StartCoroutine(CoExcuteAfterFrame(frameCount, act));;
        }
        public static Coroutine ExcuteAfterFrame(this MonoBehaviour mono, int frameCount, Action action) {
            return mono.StartCoroutine(CoExcuteAfterFrame(frameCount, action));
        }

        public static Coroutine ExcuteWhileCondition(Func<bool> condition, Action act) {
            return coHandler.StartCoroutine(CoExcuteWhileCondition(condition, act));
        }
        public static Coroutine ExcuteWhileCondition(this MonoBehaviour mono, Func<bool> condition, Action action) {
            return mono.StartCoroutine(CoExcuteWhileCondition(condition, action));
        }

        private static IEnumerator CoExcuteAfterCondition(Func<bool> condition, Action act) {
            yield return new WaitUntil(condition);
            act?.Invoke();
        }
        private static IEnumerator CoExcuteWhileCondition(Func<bool> condition, Action act) {
            while (true) {
                if (condition == null || !condition.Invoke()) yield break;
                act?.Invoke();
                yield return null;
            }
        }
        private static IEnumerator CoExcuteAfterFrame(int frameCount, Action act) {
            for (int i = 0; i < frameCount; i++) {
                yield return null;
            }
            act?.Invoke();
        }
        private static IEnumerator CoExcuteAfterSeconds(float seconds, Action act) {
            yield return new WaitForSeconds(seconds);
            act?.Invoke();
        }

    }
}

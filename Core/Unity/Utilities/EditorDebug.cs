﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    /// <summary>
    /// 에디터나 개발 모드에서만 로그를 남기는 시스템 (빌드시 제외)
    /// </summary>
    public static class EditorDebug {
        public static void Log(object message) {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
             Debug.Log(message);
            #endif
        }

        public static void LogError(object message) {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Debug.LogError(message);
            #endif
        }

        public static void LogWarning(object message) {
            #if UNITY_EDITOR || DEVELOPMENT_BUILD
            Debug.LogWarning(message);
            #endif
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BSS {
#if ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif

    [CreateAssetMenu(fileName = "NewTweenAsset", menuName = "BSS/Tween Asset",order =100)]
    public class TweenAsset : ScriptableObject {
#if ODIN_INSPECTOR
        [InfoBox("<b>Unit</b> : Y축의 1에 해당하는 값 \n<b>Freeze</b> : Tween효과에서 제외 \n[모든 Curve의 X축은 반드시 0과 1사이의 값이여야합니다.]\n ")]
#endif
        public float duration = 1f;

        [System.Serializable]
        public class AlphaTween {
            public bool isReset = true;
            public AnimationCurve curve;
        }
        [Header("Optional")]
        public bool isAlpha;
#if ODIN_INSPECTOR
        [ShowIf("isAlpha")]
#endif
        public AlphaTween alphaTween = new AlphaTween();


        [System.Serializable]
        public class ScaleTween {
            public bool isReset = true;
            public AnimationCurve curve;
            public Vector3 unitScale = Vector3.one;
            public bool freezeX;
            public bool freezeY;
            public bool freezeZ;
        }
        public bool isScale;
#if ODIN_INSPECTOR
        [ShowIf("isScale")]
#endif
        public ScaleTween scaleTween = new ScaleTween();


        [System.Serializable]
        public class PositionTween {
            public bool isReset = true;
            public AnimationCurve curve;
            public Vector3 uniPosition=Vector3.one;
            public bool freezeX;
            public bool freezeY;
            public bool freezeZ;
        }
        public bool isPosition;
#if ODIN_INSPECTOR
        [ShowIf("isPosition")]
#endif
        public PositionTween positionTween;

        [System.Serializable]
        public class RotationTween {
            public bool isReset = true;
            public AnimationCurve curve;
            public Vector3 unitRotation = Vector3.one*360f;
            public bool freezeX;
            public bool freezeY;
            public bool freezeZ;
        }
        public bool isRotation;
#if ODIN_INSPECTOR
        [ShowIf("isRotation")]
#endif
        public RotationTween rotationTween;
    }
}

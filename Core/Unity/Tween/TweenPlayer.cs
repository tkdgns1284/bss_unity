﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


namespace BSS {
    public class TweenPlayer : MonoBehaviour {
        private class PreviousData {
            public PreviousData(float _alpha, Vector3 _scale,Vector3 _position,Quaternion _rotation) {
                alpha = _alpha;
                scale = _scale;
                position = _position;
                rotation = _rotation;
            }

            public float alpha;
            public Vector3 scale;
            public Vector3 position;
            public Quaternion rotation;
        }
        public bool isPlaying { get; private set; }

        public List<TweenAsset> tweenAssets;
        public bool isLoop = false;
        public bool playOnStart = false;
        

        private int playCount;
        private int targetCount;
        private TweenAsset playElement;
        private PreviousData preData;

        private void Start() {
            if (playOnStart) {
                PlayIndex(0);
            }
        }

        public void PlayIndex(int index) {
            Play(tweenAssets[index], 1, null);
        }
        public void PlayIndex(int index, Action endAction) {
            Play(tweenAssets[index], 1, endAction);
        }
        public void PlayIndex(int index, int count) {
            Play(tweenAssets[index], count, null);
        }
        public void PlayIndex(int index, int count, Action endAction) {
            Play(tweenAssets[index], count, endAction);
        }

        public void Play(TweenAsset tweenAsset) {
            Play(tweenAsset, 1, null);
        }

        public void Play(TweenAsset tweenAsset, Action endAction) {
            Play(tweenAsset, 1, endAction);
        }
        public void Play(TweenAsset tweenAsset, int count) {
            Play(tweenAsset, count, null);
        }

        public void Play(TweenAsset element,int count,Action endAction) {
            Ensure.That(nameof(element)).IsNotNull(element);
            Ensure.That(nameof(element)).IsGreaterOrEqual(count,0);
            if (isPlaying) return;
            if (count == 0) return;
            if (element.duration <= 0f) return;

            playCount = 0;
            targetCount = count;
            preData = new PreviousData(GetAlpha(element), transform.localScale, transform.position, transform.rotation);
            StartCoroutine(CoPlayTween(element, endAction));
        }

        public void Stop() {
            if (preData == null || !isPlaying) return;
            StopAllCoroutines();
            SetAlphaInComponent(preData.alpha);
            transform.localScale = preData.scale;
            transform.position = preData.position;
            transform.rotation = preData.rotation;
            isPlaying = false;
            preData = null;
        }

        private void OnEnable() {
            
        }
        private void OnDisable() {
            if (!isPlaying) return;
            StopAllCoroutines();
            ResetAll(playElement,preData);
            isPlaying = false;
            playElement = null;
            preData = null;
        }


        IEnumerator CoPlayTween(TweenAsset element, Action endAction) {
            isPlaying = true;
            playElement = element;

            float duration = element.duration;
            
            float t = 0f;
            
            while (true) {
                float curveValue = t / duration;
                if (element.isAlpha) {
                    ApplyAlpha(element, curveValue);
                }
                if (element.isScale) {
                    ApplyScale(element, curveValue);
                }
                if (element.isPosition) {
                    ApplyPosition(element, curveValue);
                }
                if (element.isRotation) {
                    ApplyRotation(element, curveValue);
                }

                t += Time.deltaTime;
                if (t > element.duration) {
                    break;
                }
                yield return null;
            }

            ResetAll(element, preData);
            playCount += 1;

            if (isLoop || playCount < targetCount) {
                StartCoroutine(CoPlayTween(element, endAction));
                yield break;
            }

            isPlaying = false;
            playElement = null;
            preData = null;
            endAction?.Invoke();
        }

        

        private void ApplyAlpha(TweenAsset element, float curveValue) {
            AnimationCurve alphaCurve = element.alphaTween.curve;
            float alphaValue = Mathf.Clamp01(alphaCurve.Evaluate(curveValue));
            SetAlphaInComponent(alphaValue);
        }
        private void ApplyScale(TweenAsset element, float curveValue) {
            AnimationCurve scaleCurve = element.scaleTween.curve;
            float scaleValue = scaleCurve.Evaluate(curveValue);
            transform.localScale = element.scaleTween.unitScale * scaleValue;

            if (element.scaleTween.freezeX) {
                transform.localScale = transform.localScale.SetX(preData.scale.x);
            }
            if (element.scaleTween.freezeY) {
                transform.localScale = transform.localScale.SetY(preData.scale.y);
            }
            if (element.scaleTween.freezeZ) {
                transform.localScale = transform.localScale.SetZ(preData.scale.z);
            }
        }
        private void ApplyPosition(TweenAsset element, float curveValue) {
            AnimationCurve posCurve = element.positionTween.curve;
            float posValue = posCurve.Evaluate(curveValue);
            transform.position = preData.position + (element.positionTween.uniPosition * posValue);

            if (element.positionTween.freezeX) {
                transform.position = transform.position.SetX(preData.position.x);
            }
            if (element.positionTween.freezeY) {
                transform.position = transform.position.SetY(preData.position.y);
            }
            if (element.positionTween.freezeZ) {
                transform.position = transform.position.SetZ(preData.position.z);
            }
        }
        private void ApplyRotation(TweenAsset element, float curveValue) {
            AnimationCurve rotCurve = element.rotationTween.curve;
            float rotValue = rotCurve.Evaluate(curveValue);
            Vector3 sourceVector = element.rotationTween.unitRotation * rotValue;

            if (element.rotationTween.freezeX) {
                sourceVector = sourceVector.SetX(preData.rotation.eulerAngles.x);
            }
            if (element.rotationTween.freezeY) {
                sourceVector = sourceVector.SetY(preData.rotation.eulerAngles.y);
            }
            if (element.rotationTween.freezeZ) {
                sourceVector = sourceVector.SetZ(preData.rotation.eulerAngles.z);
            }
            transform.rotation = Quaternion.Euler(sourceVector);
        }

        private void ResetAll(TweenAsset element,PreviousData _preData) {
            if (element.isAlpha) {
                if (element.alphaTween.isReset) {
                    SetAlphaInComponent(_preData.alpha);
                } else {
                    ApplyAlpha(element, 1f);
                }
            }
            if (element.isScale) {
                if (element.scaleTween.isReset) {
                    transform.localScale = _preData.scale;
                } else {
                    ApplyScale(element, 1f);
                }
            }
            if (element.isPosition) {
                if (element.positionTween.isReset) {
                    transform.position = _preData.position;
                } else {
                    ApplyPosition(element, 1f);
                }
            }
            if (element.isRotation) {
                if (element.rotationTween.isReset) {
                    transform.rotation = _preData.rotation;
                } else {
                    ApplyRotation(element, 1f);
                }
            }
        }


        private void SetAlphaInComponent(float value) {
            var cg = GetComponent<CanvasGroup>();
            if (cg != null) {
                cg.alpha = value;
                return;
            }
            var render = GetComponent<SpriteRenderer>();
            if (render != null) {
                render.color = new Color(render.color.r, render.color.g, render.color.b, value);
                return;
            }
            var image = GetComponent<Image>();
            if (image != null) {
                image.color = new Color(image.color.r, image.color.g, image.color.b, value);
                return;
            }
            var text = GetComponent<Text>();
            if (text != null) {
                text.color = new Color(text.color.r, text.color.g, text.color.b, value);
                return;
            }
        }

        private float GetAlpha(TweenAsset element) {
            var cg = GetComponent<CanvasGroup>();
            if (cg != null) {
                return cg.alpha;
            }
            var render = GetComponent<SpriteRenderer>();
            if (render != null) {
                return render.color.a;
            }
            var image = GetComponent<Image>();
            if (image != null) {
                return image.color.a;
            }
            var text = GetComponent<Text>();
            if (text != null) {
                return text.color.a;
            }
            return 0f;
        }
    }
}

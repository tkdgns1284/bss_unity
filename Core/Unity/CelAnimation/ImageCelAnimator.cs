﻿using UnityEngine;
using UnityEngine.UI;

namespace BSS {
    /// <summary>
    /// Image 렌더러 애니메이터
    /// </summary>
    [RequireComponent(typeof(Image))]
    [DisallowMultipleComponent]
    public class ImageCelAnimator : BaseCelAnimator {
        protected override void ChangeFrame(Sprite frame) {
            GetComponent<Image>().sprite = frame;
        }
    }
}

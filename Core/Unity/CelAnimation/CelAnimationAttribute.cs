﻿using UnityEngine;
using System;

namespace BSS {
	[AttributeUsage(AttributeTargets.Field)]
	public class CelAnimationFieldAttribute : PropertyAttribute { }
}
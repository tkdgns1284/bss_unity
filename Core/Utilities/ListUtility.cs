﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace BSS {
    public static class ListUtility 
    {
        public static T Pop<T>(this IList<T> list) {
            Ensure.That(nameof(list)).HasItems(list);
            T item = list[list.Count - 1];
            list.Remove(item);
            return item;
        }
        public static T Peek<T>(this IList<T> list) {
            Ensure.That(nameof(list)).HasItems(list);
            return list[list.Count - 1];
        }
        public static T RandomPeek<T>(this IList<T> list) {
            return list[new Random().Next(0, list.Count-1)];
        }

        public static bool AddUnique<T>(this IList<T> collection, T item) {
            if(collection.Contains(item)) return false;
            collection.Add(item);
            return true;
        }
        public static IList<T> Swap<T>(this IList<T> list, int indexA, int indexB) {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
            return list;
        }
        public static IList<T> Fill<T>(this IList<T> list,T item,int count) {
            for (int i=0;i<count;i++) {
                list.Add(item);
            }
            return list;
        }

        /// <summary>
        /// Can allow index access just like a circular list.
        /// Eg. (list[5],7) => 1 (0,1,2,3,4,0,1) 
        /// </summary>
        public static int ValidIndex<T>(this IList<T> list, int index) {
            if(list.Count == 0)
                return 0;
            if(index < 0) {
                return list.ValidIndex(list.Count + index);
            }
            if(index >= list.Count) {
                return list.ValidIndex(index - list.Count);
            }
            return index;
        }
    }
}

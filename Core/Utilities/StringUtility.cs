﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace BSS {
    public static class StringUtility {
        public static bool IsNullOrWhiteSpace(string s) {
            return s == null || s.Trim() == string.Empty;
        }

        /// <summary>
        /// Eg. ["Item1","Item2","Item3"] , "," => "Item1,Item2,Item3"
        /// </summary>
        public static string ToSeparatedString(this IEnumerable enumerable, string separator) {
            return string.Join(separator, enumerable.Cast<object>().Select(o => o?.ToString() ?? "(null)").ToArray());
        }

    }
}
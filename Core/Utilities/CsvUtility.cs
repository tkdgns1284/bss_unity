﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;
using System.Linq;
using System;

namespace BSS {
    public static class CsvUtility 
    {
        private static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
        private static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";

        public static bool IsValidFormat(string csvText,int sizeX,int sizeY,out string[] rowLines) {
            rowLines = GetRowLines(csvText);
            if(rowLines.Length != sizeY)
                return false;
            return !rowLines.Any(ss => ss.Count(s => s == ',') != sizeX - 1);
        }

        public static string[] GetRowLines(string csvText) {
            return Regex.Split(csvText, LINE_SPLIT_RE).Where(x=>!string.IsNullOrEmpty(x)).ToArray();
        }


    }
}

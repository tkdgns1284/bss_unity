﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.Linq;

namespace BSS {
    public static class DimensionalArrayUtility 
    {
        public static void Foreach<T>(this T[,] array,Action<T> act){
            for (int x=0;x< array.GetLength(0);x++) {
                for(int y = 0; y < array.GetLength(1); y++) {
                    act?.Invoke(array[x, y]);
                }
            }
        }
        public static void Foreach<T>(this T[,] array, Action<T,int,int> act) {
            for(int x = 0; x < array.GetLength(0); x++) {
                for(int y = 0; y < array.GetLength(1); y++) {
                    act?.Invoke(array[x, y],x,y);
                }
            }
        }
        public static int LengthX<T>(this T[,] array) {
            return array.GetLength(0);
        }
        public static int LengthY<T>(this T[,] array) {
            return array.GetLength(1);
        }
        public static void Insert(this string[,] array,string csvFormat) {
            if(!CsvUtility.IsValidFormat(csvFormat,array.GetLength(0), array.GetLength(1), out string[] rowLines))
                throw new Exception("Csv text format is not valid");

            int x,y = 0;
            foreach(var rowLine in rowLines) {
                x = 0;
                foreach(var data in rowLine.Split(',')) {
                    array[x++, y] = data;
                }
                y++;
            }
        }

        


        public static string ToCsvFormat<T>(this T[,] array) {
            StringBuilder sb = new StringBuilder();
            for(int y = 0; y < array.GetLength(1); y++) {
                for(int x = 0; x < array.GetLength(0); x++) {
                    if (array[x,y]!=null) {
                        sb.Append(array[x, y].ToString());
                    }
                    if (x!=array.GetLength(0)-1) {
                        sb.Append(",");
                    }
                }
                if(y != array.GetLength(1) - 1) {
                    sb.Append(Environment.NewLine);
                }
            }
            return sb.ToString();
        }

    }
}

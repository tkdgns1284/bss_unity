﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using BSS;
using BSS.View;
using UnityEditor;

namespace BSS {
    /// <summary>
    /// 단축키 설정
    /// </summary>
    public static class HotKeyEditor  {
        private static bool IsHotKey() {
            var option = Resources.Load<BssOption>(BssOption.RESOURCE_PATH);
            return option != null ? option.isHotKey : false;
        }

        /// <summary>
        /// 현재 Scene View를 카메라로 이동시킵니다.
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Lock View to Camera #g")] // Shift+G
        public static void LockViewToCamera() {
            if(!IsHotKey()) return;
            if(SceneView.lastActiveSceneView == null)
                return;
            var camera = Camera.main;
            float screenAspect = (float)Screen.width / (float)Screen.height;
            float cameraHeight = camera.orthographicSize;
            Bounds bounds = new Bounds(camera.transform.position, new Vector3(cameraHeight * screenAspect, cameraHeight, 0));
            SceneView.lastActiveSceneView.Frame(bounds);
        }

        /// <summary>
        /// 새로운 게임오브젝트를 생성합니다. 
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Create GameObject &n")]//Alt+N
        private static void CreateGameObject() {
            if(!IsHotKey()) return;
            var obj = new GameObject();
            Undo.RegisterCreatedObjectUndo(obj, "Create GameObject");
            if(Selection.activeGameObject == null) {
                Selection.activeGameObject = obj;
                return;
            }
            obj.transform.SetParent(Selection.activeGameObject.transform);
            if(obj.GetComponentInParent<Canvas>() != null) {
                var rect = obj.AddComponent<RectTransform>();
                obj.transform.localScale = Vector3.one;
                rect.anchoredPosition = Vector2.zero;
            }
        }


        /// <summary>
        /// 선택된 게임오브젝트를 삭제합니다.
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Delete GameObject &q")]//Alt+Q
        private static void DeleteSelectObject() {
            if(!IsHotKey()) return;
            if(Selection.activeGameObject == null)
                return;
            if(EditorUtilityEx.IsPrefabAsset(Selection.activeGameObject))
                return;
            var parent = Selection.activeGameObject.transform.parent;
            foreach(var it in Selection.gameObjects) {
                if(it == null)
                    continue;
                if(EditorUtilityEx.IsPrefabAsset(it))
                    continue;
                Undo.DestroyObjectImmediate(it);
            }
            if(parent != null) {
                Selection.activeGameObject = parent.gameObject;
            }
        }

        /// <summary>
        /// 선택된 Prefab Instance를 Apply 시킵니다.
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Apply Prefab &a")]//Alt+A
        private static void ApplyPrefab() {
            if(!IsHotKey()) return;
            if(Selection.activeGameObject == null)
                return;
            if(!EditorUtilityEx.IsPrefabInstance(Selection.activeGameObject))
                return;
            EditorUtilityEx.ApplyPrefab(Selection.activeGameObject);
        }

        /// <summary>
        /// 선택된 Prefab Instance를 Prefab Revert 시킵니다.
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Revert Prefab &z")]//Alt+Z
        private static void RevertPrefab() {
            if(!IsHotKey()) return;
            if(Selection.activeGameObject == null)
                return;
            if(!EditorUtilityEx.IsPrefabInstance(Selection.activeGameObject))
                return;
            EditorUtilityEx.RevertPrefab(Selection.activeGameObject);
        }

        /// <summary>
        /// 선택된 Prefab Instance의 원본 Prefab Asset을 선택합니다.
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Select Prefab Asset &s")]//Alt+S
        public static void SelectPrefabAsset() {
            if(!IsHotKey()) return;
            if(Selection.activeGameObject == null)
                return;
            if(!EditorUtilityEx.IsPrefabInstance(Selection.activeGameObject))
                return;
            var assetPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(Selection.activeGameObject);
            var prefabAsset = (GameObject)AssetDatabase.LoadMainAssetAtPath(assetPath);
            Selection.activeObject = prefabAsset;
            EditorGUIUtility.PingObject(prefabAsset.GetInstanceID());
        }


        /// <summary>
        /// RectTransform을 풀사이즈로 변경합니다.
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Rect Full Size &f")]//Alt+F
        private static void RectSetFullSize() {
            if(!IsHotKey()) return;
            if(Selection.activeGameObject == null)
                return;
            var rect = Selection.activeGameObject.GetComponent<RectTransform>();
            if(rect == null)
                return;
            rect.SetAnchorsStretch(0f, 1f);
            rect.SetOffset(0f, 0f, 0f, 0f);
        }

        /// <summary>
        /// 선택 된 Base View를 열거나 닫습니다.
        /// </summary>
        [MenuItem("Tools/BSS/HotKey/Toggle BaseView &t")] // Alt+T
        public static void ToggleInSelectView() {
            if(!IsHotKey()) return;
            if(Selection.activeGameObject == null)
                return;
            BaseView view = Selection.activeGameObject.GetComponent<BaseView>();
            if(view == null)
                return;
            if(!view.IsVisible) {
                view.ShowOnlyCanvasGroup();
            } else {
                view.CloseOnlyCanvasGroup();
            }
        }

    }
}

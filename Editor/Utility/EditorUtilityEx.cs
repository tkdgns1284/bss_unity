﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace BSS {
    public static class EditorUtilityEx
    {
        public static void ApplyPrefab(GameObject obj) {
            PrefabUtility.ApplyPrefabInstance(obj, InteractionMode.AutomatedAction);
            string path = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(obj);
            foreach(var it in Selection.activeGameObject.GetComponents<Component>()) {
                PrefabUtility.ApplyObjectOverride(it, path, InteractionMode.AutomatedAction);
            }
        }
        public static void RevertPrefab(GameObject obj) {
            PrefabUtility.RevertObjectOverride(obj, InteractionMode.AutomatedAction);
            string path = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(obj);
            foreach(var it in Selection.activeGameObject.GetComponents<Component>()) {
                PrefabUtility.RevertObjectOverride(it, InteractionMode.AutomatedAction);
            }
        }
        public static bool IsPrefabInstance(GameObject obj) {
            var prefabStatus = PrefabUtility.GetPrefabInstanceStatus(obj);
            var prefabType = PrefabUtility.GetPrefabAssetType(obj);
            return prefabStatus == PrefabInstanceStatus.Connected && prefabType == PrefabAssetType.Regular;
        }

        public static bool IsPrefabAsset(GameObject obj) {
            var prefabStatus = PrefabUtility.GetPrefabInstanceStatus(obj);
            var prefabType = PrefabUtility.GetPrefabAssetType(obj);
            return prefabStatus == PrefabInstanceStatus.NotAPrefab && prefabType == PrefabAssetType.Regular;
        }
    }

}
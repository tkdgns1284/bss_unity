﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace BSS {
#if ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif
    [System.Serializable]
    public class ObservableData<T>  {
#if ODIN_INSPECTOR
        [ReadOnly]
#endif
        [SerializeField]
        private T _data;
        public T data {
            get => _data;
            set {
                Change(value);
            }
        }
        public event Action<T> OnChanged;
        public void Change(T setData) {
            if(setData == null && _data == null)
                return;
            if(setData != null && setData.Equals(_data))
                return;
            _data = setData;
            OnChanged?.Invoke(_data);
        }
    }
}

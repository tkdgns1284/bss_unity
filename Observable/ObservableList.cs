﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace BSS {
#if ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif
    [System.Serializable]
    public class ObservableList<T> {
#if ODIN_INSPECTOR
        [ReadOnly]
#endif
        [SerializeField]
        private List<T> _data;
        public IEnumerable<T> data {
            get => _data;
        }
        public T this[int index]=> _data[index];

        public event Action OnChanged;

        public void Add(T item) {
            _data.Add(item);
            OnChanged?.Invoke();
        }
        public void Remove(T item) {
            if (_data.Remove(item)) {
                OnChanged?.Invoke();
            }
        }
        public void RemoveAt(int index) {
            _data.RemoveAt(index);
            OnChanged?.Invoke();
        }

        public void Clear() {
            _data.Clear();
            OnChanged?.Invoke();
        }

    }
}

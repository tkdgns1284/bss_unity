﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BSS {
    [CreateAssetMenu(fileName ="Option",menuName ="BSS/BssOption")]
    public class BssOption : ScriptableObject
    {
        public static string RESOURCE_PATH = "Option/BssOption";

        public bool isHotKey=true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System;

namespace BSS {
    /// <summary>
    /// 이차원 배열 Class (1차원 List기반) 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [System.Serializable]
    public class Matrix<T> : IEnumerable<(int x,int y,T item)> {
        public Matrix(int x,int y) {
            Ensure.That(nameof(x)).IsGreater(x, 0);
            Ensure.That(nameof(y)).IsGreater(y, 0);
            Size = (x, y);
            for (int i=0;i<x*y;i++) {
                data.Add(default);
            }
            data.Fill(default,x*y);
        }
        public Matrix(int x, int y, T val) : this(x, y) {
            SetAll(val);
        }
        public Matrix(IList<T[]> source)  {
            Ensure.That("x").IsGreater(source.Count, 0);
            Ensure.That("y").IsGreater(source[0].Length, 0);
            Size = (source.Count, source[0].Length);
            foreach (var it in source) {
                foreach (var jt in it) {
                    data.Add(jt);
                }
            }
        }
        public Matrix(T[][] source) {
            Ensure.That("x").IsGreater(source.Length, 0);
            Ensure.That("y").IsGreater(source[0].Length, 0);
            Size = (source.Length, source[0].Length);
            foreach(var it in source) {
                foreach(var jt in it) {
                    data.Add(jt);
                }
            }
        }
        public Matrix(T[,] source) {
            Ensure.That("x").IsGreater(source.GetLength(0), 0);
            Ensure.That("y").IsGreater(source.GetLength(1), 0);
            Size = (source.GetLength(0), source.GetLength(1));
            foreach(var it in source) {
                data.Add(it);
            }
        }

        public Matrix(IList<List<T>> source) {
            Ensure.That("x").IsGreater(source.Count, 0);
            Ensure.That("y").IsGreater(source[0].Count, 0);
            Size = (source.Count, source[0].Count);
            foreach(var it in source) {
                foreach(var jt in it) {
                    data.Add(jt);
                }
            }
        }

        public T this[int x,int y] {
            get => data[x*Size.y+y];
            set => data[x * Size.y + y] = value;
        }
        public T this[(int x, int y) pair] {
            get => data[pair.x * Size.y + pair.y];
            set => data[pair.x * Size.y + pair.y] = value;
        }

        public readonly (int x, int y) Size;
        public int Count => data.Count;
        public List<T> data = new List<T>();//내부구조는 1차원배열

        /// <summary>
        /// 모든 요소를 item으로 변경합니다.
        /// </summary>
        public void SetAll(T item) {
            for (int i=0;i<Count;i++) {
                data[i] = item;
            }
        }
        /// <summary>
        /// 모든 요소를 transFunc의 Return 값으로 변경합니다.
        /// </summary>
        public void SetAll(Func<(int x,int y,T item),T> transFunc) {
            foreach (var it in GetEnumerable()) {
                this[it.x, it.y] = transFunc(it);
            }
        }
        /// <summary>
        /// Predicate에 해당하는 요소를 item으로 변경합니다.
        /// </summary>
        public void Set(T item,Func<(int x,int y,T item), bool> predicate) {
            foreach(var it in GetEnumerable()) {
                if(!predicate(it))
                    continue;
                this[it.x, it.y] = item;
            }
        }
        /// <summary>
        /// Predicate에 해당하는 요소를 transFunc의 Return 값으로 변경합니다.
        /// </summary>
        public void Set(Func<(int x, int y, T item), T> transFunc, Func<(int x, int y, T item), bool> predicate) {
            foreach(var it in GetEnumerable()) {
                if(!predicate(it))
                    continue;
                this[it.x, it.y] = transFunc(it);
            }
        }

        /// <summary>
        /// (x,y,item) 튜플의 이터레이터를 반환합니다.
        /// </summary>
        public IEnumerable<(int x,int y,T item)> GetEnumerable() {
            for (int i=0;i<Size.x;i++) {
                for (int j=0;j<Size.y;j++) {
                    yield return (i,j,this[i, j]);
                }
            }
        }

        public IEnumerator<(int x, int y, T item)> GetEnumerator() {
            return GetEnumerable().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            return GetEnumerable().GetEnumerator();
        }

        private StringBuilder sb;
        public override string ToString() {
            if(sb == null)
                sb = new StringBuilder();
            sb.Clear();
            int x = 0;
            foreach (var it in GetEnumerable()) {
                if(x != it.x) {
                    sb.Append("\n");
                    x = it.x;
                }
                sb.Append($"[{it.x},{it.y}] :{it.item} ");
            }
            return sb.ToString();
        }
    }

    public static class MatrixUtility {
        public static Matrix<T> ToMatrix<T>(this IEnumerable<(int x, int y, T item)> iterator) {
            int sizeX = 0;
            int sizeY = 0;
            foreach(var it in iterator) {
                if(sizeX < it.x)
                    sizeX = it.x;
                if(sizeY < it.y)
                    sizeY = it.y;
            }
            var matrix = new Matrix<T>(sizeX + 1, sizeY + 1);
            foreach(var it in iterator) {
                matrix[it.x, it.y] = it.item;
            }
            return matrix;
        }
        #region AddAll (All Numeric Type)
        public static Matrix<int> AddAll(this Matrix<int> matrix,int val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item + val;
            }
            return matrix;
        }
        public static Matrix<long> AddAll(this Matrix<long> matrix, long val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item + val;
            }
            return matrix;
        }
        public static Matrix<float> AddAll(this Matrix<float> matrix, float val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item + val;
            }
            return matrix;
        }
        public static Matrix<double> AddAll(this Matrix<double> matrix, double val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item + val;
            }
            return matrix;
        }
        #endregion

        #region MultiplyAll (All Numeric Type)
        public static Matrix<int> MultiplyAll(this Matrix<int> matrix, int val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item * val;
            }
            return matrix;
        }
        public static Matrix<long> MultiplyAll(this Matrix<long> matrix, long val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item * val;
            }
            return matrix;
        }
        public static Matrix<float> MultiplyAll(this Matrix<float> matrix, float val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item * val;
            }
            return matrix;
        }
        public static Matrix<double> MultiplyAll(this Matrix<double> matrix, double val) {
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item * val;
            }
            return matrix;
        }
        #endregion

        #region DivideAll (All Numeric Type)
        public static Matrix<int> DivideAll(this Matrix<int> matrix, int val) {
            Ensure.That(nameof(val)).IsNot(val, 0);
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item / val;
            }
            return matrix;
        }
        public static Matrix<long> DivideAll(this Matrix<long> matrix, long val) {
            Ensure.That(nameof(val)).IsNot(val, 0);
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item / val;
            }
            return matrix;
        }
        public static Matrix<float> DivideAll(this Matrix<float> matrix, float val) {
            Ensure.That(nameof(val)).IsNot(val, 0f);
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item / val;
            }
            return matrix;
        }
        public static Matrix<double> DivideAll(this Matrix<double> matrix, double val) {
            Ensure.That(nameof(val)).IsNot(val, 0d);
            foreach(var it in matrix) {
                matrix[it.x, it.y] = it.item / val;
            }
            return matrix;
        }
        #endregion
    }
}

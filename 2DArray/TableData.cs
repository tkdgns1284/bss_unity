﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System;

namespace BSS {
    /// <summary>
    /// Table 형태의 정보를 가지고 있는 Data Class 
    /// (첫번째 행은 Key값)
    /// Eg.  ----------------------
    ///      | Id,Name,Price      | 
    ///      | 0,"Bag",100        |
    ///      | 1,"Mouse",200      | 
    ///      | 2,"Keyboard",300   |
    ///      ----------------------
    /// </summary>
    [System.Serializable]
    public class TableData {
        static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
        static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";


        private List<string> _keys;
        public IEnumerable<string> keys => _keys.AsEnumerable();
        
        private List<Dictionary<string, string>> _maps = new List<Dictionary<string, string>>();
        public IEnumerable<Dictionary<string,string>> maps=>_maps;

        private Dictionary<string, List<string>> _values = new Dictionary<string, List<string>>();



        public TableData(string csvFormat) {
            string[] lines = Regex.Split(csvFormat, LINE_SPLIT_RE);

            _keys = Regex.Split(lines[0], SPLIT_RE).ToList();
            foreach (var key in _keys) {
                _values[key] = new List<string>();
            }

            for (var i = 1; i < lines.Length; i++) {
                if (string.IsNullOrEmpty(lines[i])) continue;
                var dic = new Dictionary<string, string>();
                string[] dataList = Regex.Split(lines[i], SPLIT_RE);
                for (int j = 0; j < dataList.Length; j++) {
                    string key = _keys[j];
                    string data = dataList[j];
                    dic[key] = data;
                    _values[key].Add(data);
                }
                _maps.Add(dic);
            }
        }

        public IEnumerable<string> GetValues(string key) {
            return _values[key];
        }

        public Dictionary<string,string> GetData(string key, Func<string, bool> predicate) {
            var keyValues = _values[key].FindAll(x => predicate(x));
            return _maps.Find(x => keyValues.Exists(xx => xx == x[key]));
        }
        

        public IEnumerable<IDictionary<string, string>> GetDataAll(string key, Func<string, bool> predicate) {
            var keyValues= _values[key].FindAll(x => predicate(x));
            return _maps.FindAll(x => keyValues.Exists(xx => xx == x[key]));
        }
    }
}
